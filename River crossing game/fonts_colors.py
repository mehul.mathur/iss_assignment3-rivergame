from random import randint
from random import seed, random
import pygame
from obstacles import *


"""Imports the pygame modules , random library modules and also imports all
definitions and functions from the config file obstacles.py"""


pygame.init()

"""Defines all the colors in rgb or hex values"""

green = (0, 255, 0)
blue = (0, 0, 128)
yellow = (243, 229, 9)

backcolor = pygame.color.Color('#33DAFF')
rectcolor = pygame.color.Color('#E89400')


"""This block of code defines all the fonts and their styles"""

font = pygame.font.Font('freesansbold.ttf', 32)
font2 = pygame.font.Font(None, 29)
start = font.render('START', True, yellow, None)
startRect = start.get_rect()
startRect.center = (1200, 970)
end = font.render('END', True, yellow, None)
endRect = end.get_rect()
endRect.center = (1200, 20)
win1 = font.render('Player 1 wins!!', True, green, blue)
win2 = font.render('Player 2 wins!!', True, green, blue)
winRect1 = win1.get_rect()
winRect1.center = (width // 2, height // 2)
winRect2 = win2.get_rect()
winRect2.center = (width // 2, height // 2)
text = font.render('YOU LOSE!!', True, green, blue)
textRect = text.get_rect()
textRect.center = (width // 2, height // 2)
score = font.render('Score: 0', True, green, blue)
scoreRect = score.get_rect()
scoreRect.center = (100, 20)
