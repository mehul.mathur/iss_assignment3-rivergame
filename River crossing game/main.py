from random import randint
from random import seed, random
import pygame
from obstacles import *
from fonts_colors import *


"""Imports the pygame modules , random library modules and also imports all
definitions and functions from the config files obstacles.py and
fonts_colors.py"""


pygame.init()


def obstacle_draw(obstacles):
    for obstacle in obstacles:
        obstacle.draw()


"""Function to draw all obstacles present in the obstacle(sprite list"""


mov_obstacle_list = []
obst_list = []
all_sprites_list = []


sonic_img = pygame.image.load(
    'runner.png')
sonic_img = pygame.transform.scale(sonic_img, (70, 70))
sonic_img = pygame.transform.flip(sonic_img, True, False)
knuckles_img = pygame.image.load(
    'knuckles.png')
knuckles_img = pygame.transform.scale(knuckles_img, (70, 70))

background = pygame.image.load('bg_img.jpg')
#suggested by pooja govind desur
background = pygame.transform.scale(background,(width,height))


def sonic(x, y):
    screen.blit(sonic_img, (x, y))


def knuckles(a, b):
    screen.blit(knuckles_img, (a, b))


"""Functions to display player1(sonic) and player2(knuckles) respectively
according to the coordinates given as parameters"""

x = (width * 0.45)
y = (height * 0.93)

x2 = (width * 0.75)
y2 = (height * 0.005)

clock = pygame.time.Clock()
# font2=pygame.font.Font('')

sc = '5'


#screen.fill(backcolor)
screen.blit(background,(0,0))

pygame.draw.rect(screen, (rectcolor), (0, 0, 1320, 72))
pygame.draw.rect(screen, (rectcolor), (0, 232, 1320, 72))
pygame.draw.rect(screen, (rectcolor), (0, 464, 1320, 72))
pygame.draw.rect(screen, (rectcolor), (0, 696, 1320, 72))
pygame.draw.rect(screen, (rectcolor), (0, 928, 1320, 72))

"""This block draws the background and rectangles"""

for i in range(0, 5):
    sh1 = Shark(i * 250, 800)
    if i == 0:
        egg = Eggman(0, 688)
    elif i == 4:
        egg = Eggman(1100, 688)
    else:
        egg = Eggman(randint(0, 1175), 688)
    all_sprites_list.append(egg)
    all_sprites_list.append(sh1)
    mov_obstacle_list.append(sh1)
    obst_list.append(egg)

for i in range(0, 5):
    sh1 = Shark(i * 280, 570)
    if i == 0:
        egg = Eggman(0, 458)
    elif i == 4:
        egg = Eggman(1100, 458)
    else:
        egg = Eggman(randint(0, 1175), 458)
    all_sprites_list.append(egg)
    all_sprites_list.append(sh1)
    mov_obstacle_list.append(sh1)
    obst_list.append(egg)

for i in range(0, 5):
    sh1 = Shark(i * 280, 340)
    if i == 0:
        egg = Eggman(0, 228)
    elif i == 4:
        egg = Eggman(1100, 228)
    else:
        egg = Eggman(randint(0, 1175), 228)

    all_sprites_list.append(egg)
    all_sprites_list.append(sh1)
    mov_obstacle_list.append(sh1)
    obst_list.append(egg)

for i in range(0, 5):
    sh1 = Shark(i * 280, 110)
    all_sprites_list.append(sh1)
    mov_obstacle_list.append(sh1)


"""All these loops add the respective obstacles(moving or stationary) to
their lists along with the position they are supposed to be displayed at"""

pygame.display.flip()
x_change = 0
y_change = 0
x2_change = 0
y2_change = 0
done = False
sc = '0'
score = font.render('Score: ' + sc, True, green, blue)
round = 0
st_pos = 970
end_pos = 20
time = 0
player_1_time = 0
player_2_time = 0
player_1_score = 0
player_2_score = 0
start_time_flag = 0
speed1 = 1.3
speed2 = 1.3
fail1 = 0
won1 = 0

pygame.mixer.music.load("sonic_song.mp3")
pygame.mixer.music.play(-1)

"""Loads the music and plays it in an infinite loop"""


while not done:

    if not round:
        if y + 20 <= 928:
            start_time_flag = 1

    else:
        if y2 + 35 >= 70:
            start_time_flag = 1

    if start_time_flag:
        time += 1

        """This assures that the timer only starts when the players cross the first rectangle fully"""

    startRect.center = (1200, st_pos)
    endRect.center = (1200, end_pos)
    # print(sc)
    score = font.render('Score: ' + sc, True, green, blue)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if not round:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -5
                    y_change = 0
                    sonic_img = pygame.transform.flip(sonic_img, True, False)
                elif event.key == pygame.K_RIGHT:
                    x_change = 5
                    y_change = 0
                    sonic_img = pygame.transform.flip(sonic_img, True, False)

                if event.key == pygame.K_UP:
                    x_change = 0
                    y_change = -5

                elif event.key == pygame.K_DOWN:
                    x_change = 0
                    y_change = 5

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    y_change = 0

        else:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    x2_change = -5
                    y2_change = 0
                    knuckles_img = pygame.transform.flip(
                        knuckles_img, True, False)
                elif event.key == pygame.K_d:
                    x2_change = 5
                    y2_change = 0
                    knuckles_img = pygame.transform.flip(
                        knuckles_img, True, False)

                if event.key == pygame.K_w:
                    x2_change = 0
                    y2_change = -5

                elif event.key == pygame.K_s:
                    x2_change = 0
                    y2_change = 5

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a or event.key == pygame.K_d:
                    x2_change = 0
                elif event.key == pygame.K_w or event.key == pygame.K_s:
                    y2_change = 0

    if not round:
        x += x_change
        y += y_change
    else:
        x2 += x2_change
        y2 += y2_change

    """These for loops are the event handling mechanisms for controling the
    players with arrow keys and wasd keys respectively"""

    #screen.fill(backcolor)
    screen.blit(background,(0,0))
    pygame.draw.rect(screen, (rectcolor), (0, 0, 1320, 72))
    pygame.draw.rect(screen, (rectcolor), (0, 232, 1320, 72))
    pygame.draw.rect(screen, (rectcolor), (0, 464, 1320, 72))
    pygame.draw.rect(screen, (rectcolor), (0, 696, 1320, 72))
    pygame.draw.rect(screen, (rectcolor), (0, 928, 1320, 72))

    if x < 0:
        x = 0
    if x > (width - 70):
        x = (width - 70)

    if y < 0:
        y = 0
    if y > (height - 70):
        y = (height - 70)

    if x2 < 0:
        x2 = 0
    if x2 > (width - 70):
        x2 = (width - 70)

    if y2 < 0:
        y2 = 0
    if y2 > (height - 70):
        y2 = (height - 70)

    """Gives boundaries so player doesnt go beyond the screen"""

    sonic(x, y)
    player_hitbox = (x + 13, y + 5, 50, 55)
    # pygame.draw.rect(screen, (255, 0, 0), player_hitbox, 2)
    player_x = player_hitbox[0]
    player_y = player_hitbox[1]
    knuckles(x2, y2)
    player2_hitbox = (x2 + 13, y2 + 5, 50, 60)
    # pygame.draw.rect(screen, (255, 0, 0), player2_hitbox, 2)
    player2_x = player2_hitbox[0]
    player2_y = player2_hitbox[1]
    obstacle_draw(obst_list)
    obstacle_draw(mov_obstacle_list)
    screen.blit(score, scoreRect)
    screen.blit(start, startRect)
    screen.blit(end, endRect)
    l = len(mov_obstacle_list)

    """Defines hitboxes for the sprites as well as the players for collision
    detection"""

    for i in range(0, l):
        if not round:
            mov_obstacle_list[i].update(speed1)

        else:
            mov_obstacle_list[i].update(speed2)

    """Moves the sharks"""

    if not round:

        if player_y > 800:
            sc = '0'

        if player_y < 800 and player_y > 696:
            sc = '10'

        if player_y < 696 and player_y > 570:
            sc = '15'

        if player_y < 570 and player_y > 464:
            sc = '25'

        if player_y < 464 and player_y > 340:
            sc = '30'

        if player_y < 340 and player_y > 232:
            sc = '40'

        if player_y < 232 and player_y > 110:
            sc = '45'

        if player_y < 110 and player_y > 0:
            sc = '55'

        """Updates the scores according to position of player1"""

        if y == 0:
            player_1_score = int(sc)
            player_1_time = time
            print(player_1_time)
            time = 0
            # screen.blit(win, winRect)
            pygame.display.flip()
            pygame.time.delay(1500)
            x = (width * 0.45)
            y = (height * 0.93)
            sc = '0'
            st_pos = 20
            end_pos = 970
            start_time_flag = 0
            round = 1
            x2_change = 0
            y2_change = 0
            won1 = 1

        """Code for when player1 has reached the end"""

    else:

        if player2_y + 60 < 175:
            sc = '0'

        if player2_y + 60 > 175 and player2_y + 60 < 307:
            sc = '10'

        if player2_y + 60 > 307 and player2_y + 60 < 405:
            sc = '15'

        if player2_y + 60 > 405 and player2_y + 60 < 539:
            sc = '25'

        if player2_y + 60 > 539 and player2_y + 60 < 635:
            sc = '30'

        if player2_y + 60 > 635 and player2_y + 60 < 771:
            sc = '40'

        if player2_y + 60 > 771 and player2_y + 60 < 865:
            sc = '45'

        if player2_y + 60 > 865 and player2_y + 60 < height:
            sc = '55'

        """Updates the score according to the position of player2"""

        if y2 + 70 == height:

            start_time_flag = 0
            player_2_score = int(sc)
            player_2_time = time
            print(player_2_time)

            if not fail1:
                if player_2_score == player_1_score:
                    if player_1_time >= player_2_time:
                        screen.blit(win2, winRect2)
                        speed2 += 0.3
                    else:
                        screen.blit(win1, winRect1)
                        speed1 += 0.3
                else:
                    if player_1_score > player_2_score:
                        screen.blit(win1, winRect1)
                        speed1 += 0.3
                    else:
                        screen.blit(win2, winRect2)
                        speed2 += 0.3

            else:
                screen.blit(win2, winRect2)

            fail1 = 0
            won1 = 0

            time = 0

            pygame.display.flip()
            pygame.time.delay(1500)
            x2 = (width * 0.75)
            y2 = (height * 0.005)
            sc = '0'
            st_pos = 970
            end_pos = 20
            round = 0
            x_change = 0
            y_change = 0

        """Code for when player2 reaches the end"""

    for ent in obst_list:

        if not round:
            if player_x <= ent.hitbox[0] + 115 and player_x > ent.hitbox[0]:
                if player_y > ent.hitbox[1] and player_y <= ent.hitbox[1] + 80:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    # screen.blit(text, textRect)
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x + 50 >= ent.hitbox[0] and player_x + 50 < ent.hitbox[
                    0] + 115:
                if player_y > ent.hitbox[1] and player_y <= ent.hitbox[1] + 80:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    # screen.blit(text, textRect)
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x <= ent.hitbox[0] + 115 and player_x > ent.hitbox[0]:
                if player_y + 55 >= ent.hitbox[1] and player_y + 55 < \
                        ent.hitbox[1] + 80:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    # screen.blit(text, textRect)
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x + 50 <= ent.hitbox[0] + 115 and player_x + 50 > \
                    ent.hitbox[0]:
                if player_y + 55 >= ent.hitbox[1] and player_y + 55 < \
                        ent.hitbox[1] + 80:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    # screen.blit(text, textRect)
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x + 50 >= ent.hitbox[0] and player_x + 50 < ent.hitbox[
                    0] + 115:
                if player_y <= ent.hitbox[1] and player_y + 55 >= ent.hitbox[
                        1] + 80:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    # screen.blit(text, textRect)
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            """This code is to handle collision detection of player1 with
            stationary objects"""

        else:

            if player2_x <= ent.hitbox[0] + 115 and player2_x > ent.hitbox[0]:
                if player2_y > ent.hitbox[1] and player2_y <= ent.hitbox[
                        1] + 80:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0

                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x + 50 >= ent.hitbox[0] and player2_x + 50 < ent.hitbox[
                    0] + 115:
                if player2_y > ent.hitbox[1] and player2_y <= ent.hitbox[
                        1] + 80:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x <= ent.hitbox[0] + 115 and player2_x > ent.hitbox[0]:
                if player2_y + 60 >= ent.hitbox[1] and player2_y + 60 < \
                        ent.hitbox[1] + 80:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x + 50 <= ent.hitbox[0] + 115 and player2_x + 50 > \
                    ent.hitbox[0]:
                if player2_y + 60 >= ent.hitbox[1] and player2_y + 60 < \
                        ent.hitbox[1] + 80:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x + 50 >= ent.hitbox[0] and player2_x + 50 < ent.hitbox[
                    0] + 115:
                if player2_y <= ent.hitbox[1] and player2_y + 60 >= ent.hitbox[
                        1] + 80:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0

                    break

            """This code is to handle collision detection of player2 with
                        stationary objects"""

    for ent in mov_obstacle_list:

        if not round:
            if player_x <= ent.hitbox[0] + 110 and player_x > ent.hitbox[0]:
                if player_y > ent.hitbox[1] and player_y <= ent.hitbox[1] + 39:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x + 50 >= ent.hitbox[0] and player_x + 50 < ent.hitbox[
                    0] + 110:
                if player_y > ent.hitbox[1] and player_y <= ent.hitbox[1] + 39:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x <= ent.hitbox[0] + 110 and player_x > ent.hitbox[0]:
                if player_y + 55 >= ent.hitbox[1] and player_y + 55 < \
                        ent.hitbox[1] + 39:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x + 50 <= ent.hitbox[0] + 110 and player_x + 50 > \
                    ent.hitbox[0]:
                if player_y + 55 >= ent.hitbox[1] and player_y + 55 < \
                        ent.hitbox[1] + 39:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x + 50 >= ent.hitbox[0] and player_x + 50 < ent.hitbox[
                    0] + 110:
                if player_y <= ent.hitbox[1] and player_y + 55 >= ent.hitbox[
                        1] + 39:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            if player_x <= ent.hitbox[0] + 110 and player_x > ent.hitbox[0]:
                if player_y < ent.hitbox[1] and player_y + 55 >= ent.hitbox[
                        1] + 39:
                    player_1_score = int(sc)
                    player_1_time = time
                    time = 0
                    start_time_flag = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x = (width * 0.45)
                    y = (height * 0.93)
                    sc = '0'
                    st_pos = 20
                    end_pos = 970
                    round = 1
                    x2_change = 0
                    y2_change = 0
                    fail1 = 1
                    break

            """This code is to handle collision detection of player1 with
                        moving objects"""

        else:

            if player2_x <= ent.hitbox[0] + 110 and player2_x > ent.hitbox[0]:
                if player2_y > ent.hitbox[1] and player2_y <= ent.hitbox[
                        1] + 39:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x + 50 >= ent.hitbox[0] and player2_x + 50 < ent.hitbox[
                    0] + 110:
                if player2_y > ent.hitbox[1] and player2_y <= ent.hitbox[
                        1] + 39:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x <= ent.hitbox[0] + 110 and player2_x > ent.hitbox[0]:
                if player2_y + 60 >= ent.hitbox[1] and player2_y + 60 < \
                        ent.hitbox[1] + 39:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x + 50 <= ent.hitbox[0] + 110 and player2_x + 50 > \
                    ent.hitbox[0]:
                if player2_y + 60 >= ent.hitbox[1] and player2_y + 60 < \
                        ent.hitbox[1] + 39:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x + 50 >= ent.hitbox[0] and player2_x + 50 < ent.hitbox[
                    0] + 110:
                if player2_y <= ent.hitbox[1] and player2_y + 60 >= ent.hitbox[
                        1] + 39:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            if player2_x <= ent.hitbox[0] + 110 and player2_x > ent.hitbox[0]:
                if player2_y < ent.hitbox[1] and player2_y + 55 >= ent.hitbox[
                        1] + 39:
                    player_2_score = int(sc)
                    player_2_time = time
                    time = 0
                    start_time_flag = 0
                    if not won1:
                        if player_2_score == player_1_score:
                            if player_1_time >= player_2_time:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                            else:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                        else:
                            if player_1_score > player_2_score:
                                screen.blit(win1, winRect1)
                                speed1 += 0.3
                            else:
                                screen.blit(win2, winRect2)
                                speed2 += 0.3
                        pygame.display.flip()

                    else:
                        screen.blit(win1, winRect1)

                    won1 = 0
                    fail1 = 0
                    pygame.display.flip()
                    pygame.time.delay(1500)
                    x2 = (width * 0.75)
                    y2 = (height * 0.005)
                    sc = '0'
                    st_pos = 970
                    end_pos = 20
                    round = 0
                    x_change = 0
                    y_change = 0
                    break

            """This code is to handle collision detection of player2 with
                        moving objects"""

    pygame.display.flip()
    """Updates everything"""
    clock.tick(1020)
    """Clock tick speed (frames per second)"""

pygame.quit()

"""Quits the game once close is pressed"""
